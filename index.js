const service = require('restana')({
    ignoreTrailingSlash: true
})
const Datastore = require('nedb-promises')

const start = async () => {
    let db = Datastore.create('./db.db')
    await db.load();

    service.get('/:port', async (req, res) => {
        const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
        const port = req.params.port
        const address = `${ip}:${port}`;
        const data = await db.find({address: { $ne: address }}).exec();
        res.body = data
        res.send()
    })

    service.get('/connect/:port', async (req, res) => {
        const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
        const port = req.params.port
        const address = `${ip}:${port}`;
        console.log('connected', address);
        await db.update({address}, { $set: {address} }, { upsert: true }).catch((err) => console.log(err));
        res.body = 'connected'
        res.send()
    })

    service.get('/disconnect/:port', async (req, res) => {
        const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
        const port = req.params.port
        const address = `${ip}:${port}`;
        console.log('disconnected', address);
        await db.remove({ address });
        res.body = 'disconnected'
        res.send()
    })

    const port = process.env.PORT || 5050
    service.start(port).then(() => { console.log(`Server Is Listening on port ${port}`) })
}

start();